import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

import java.sql.*;
import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class JDBCExampleMySql {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static PrintStream out = System.out;

    public static void main(String[] args) {
        try {
            mostrarMenu();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void mostrarMenu() throws IOException {
        int opcion;
        opcion = -1;
        do {
            out.println("1.Listar Personas");
            out.println("2.Registrar Persona");
            out.println("3.Modificar Persona ");
            out.println("4. Eliminar Persona ");
            out.println("5.Listar Mascota");
            out.println("6.Registrar Mascota");
            out.println("7.Modificar Mascota ");
            out.println("8. Eliminar Mascota ");
            out.println("0.Salir");
            out.println("Digite la opción que dese");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        } while (opcion != 0);
    }

    //Rutina para procesar la opción seleccionada por el usuario
    public static void procesarOpcion(int pOpcion)
            throws java.io.IOException {
        switch (pOpcion) {
            case 1:
                String hacer = "si";
                listar(hacer);
                break;
            case 2:
                registrar();
                break;
            case 3:
                modificar();
                break;
            case 4:
                eliminar();
                break;
            case 5:
                listarMascota();
                break;
            case 6:
                registrarMascota();
                break;
            case 7:
                modificarMascota();
                break;
            case 8:
                eliminarMascota();
                break;
            case 0:
                out.println("Adiós");
                break;
            default:
                out.println("Opción inválida");
        }
    }

    public static ArrayList<Persona> listar(String hacer) {
        ArrayList<Persona> personas = new ArrayList<>();
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = null;
            Statement stmt = null;
            ResultSet rs = null;
            ArrayList<Persona> lista = new ArrayList<>();
            conn = DriverManager.getConnection("jdbc:mysql://localhost/prueba?" +
                    "user=root&password=root");
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM PERSONA");
            while (rs.next()) {
                Persona p = new Persona();
                p.setNombre(rs.getString("NOMBRE"));
                p.setApellido(rs.getString("APELLIDOS"));
                p.setEdad(rs.getInt("EDAD"));
                p.setIdPersona(rs.getInt("ID"));
                lista.add(p);
            }
            personas = lista;
            //se imprime el Array list
            if (hacer.equals("si")){
                for (Persona p : lista) {
                    out.println(p.toString());
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return personas;
    }

    public static void listarMascota() {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations

            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = null;
            Statement stmt = null;
            ResultSet rs = null;
            ArrayList<Mascota> lista = new ArrayList<>();
            conn = DriverManager.getConnection("jdbc:mysql://localhost/prueba?" +
                    "user=root&password=root");
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM MASCOTA");
            while (rs.next()) {
                Mascota p = new Mascota();
                p.setNombre(rs.getString("NOMBRE"));
                p.setRaza(rs.getString("RAZA"));
                Date fecha = new Date(rs.getDate("FEC_NACIMIENTO").getTime());
                p.setFecNacimiento(fecha.toLocalDate());
                p.setIdMascota(rs.getInt("ID_MASCOTA"));
                int dato = rs.getInt("ID_DUENO");
                String hacer = "no";
                ArrayList<Persona> personas = listar(hacer);
                for (Persona data:personas) {
                    if(data.getIdPersona()==dato){
                        p.setDeunno(data);
                    }
                }
                lista.add(p);
            }

            //se imprime el Array list
            for (Mascota p : lista) {
                out.println(p.toString());
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void registrar()
            throws IOException {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = null;
            Statement stmt = null;
            out.println("Digite el nombre de la persona");
            String nombre = in.readLine();
            out.println("Digite los apellidos");
            String apelldio = in.readLine();
            out.println("Digite la edad");
            int edad = Integer.parseInt(in.readLine());
            out.println("Digite la cedula sin guiones");
            int cedula = Integer.parseInt(in.readLine());
            conn = DriverManager.getConnection("jdbc:mysql://localhost/prueba?" +
                    "user=root&password=root");
            stmt = conn.createStatement();
            stmt.execute("INSERT INTO PERSONA (NOMBRE,APELLIDOS,EDAD,ID) VALUES " +
                    "('" + nombre + "','" + apelldio + "'," +
                    edad + "," + cedula + ")");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void registrarMascota()
            throws IOException {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = null;
            Statement stmt = null;
            out.println("Digite el nombre de la mascota");
            String nombre = in.readLine();
            out.println("Digite la raza");
            String raza = in.readLine();
            out.println("Digite la la identificacion de la mascota");
            int idMascota = Integer.parseInt(in.readLine());
            out.println("Digite la la identificacion del duenno");
            int idDueno = Integer.parseInt(in.readLine());
            int dia;
            int mes;
            int anno;
            out.println("Digite el dia ");
            dia = Integer.parseInt(in.readLine());
            out.println("digite el mes");
            mes = Integer.parseInt(in.readLine());
            out.println("Digite el anno");
            anno = Integer.parseInt(in.readLine());
            LocalDate fecNac = LocalDate.of(anno, mes, dia);
            conn = DriverManager.getConnection("jdbc:mysql://localhost/prueba?" +
                    "user=root&password=root");
            stmt = conn.createStatement();
            stmt.execute("INSERT INTO MASCOTA (ID_MASCOTA, NOMBRE,RAZA, FEC_NACIMIENTO,ID_DUENO) VALUES " +
                    "("+idMascota+",'" + nombre + "','" + raza  + "','" +
                    fecNac + "'," + idDueno + ")");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void modificar() {
        try {
            // The newInstance() call is a work around for someClass.forName("com.mysql.jdbc.Driver").newInstance();
            // broken Java implementations
            String hacer = "si";
            listar(hacer);
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = null;
            Statement stmt = null;
            out.println("Digite la cedula de la persona que desea modificar");
            int cedula = Integer.parseInt(in.readLine());
            out.println("Digite el nuevo nombnre");
            String nombre = in.readLine();
            out.println("Digite el nuevo apellido");
            String apellido = in.readLine();
            out.println("Digite la nueva edad");
            int edad = Integer.parseInt(in.readLine());
            conn = DriverManager.getConnection("jdbc:mysql://localhost/prueba?" +
                    "user=root&password=root");

            stmt = conn.createStatement();
            String cmdText = "UPDATE PERSONA SET NOMBRE = '" +
                    nombre + "',APELLIDO = '" + apellido + "', EDAD =" + edad+
                    " WHERE ID = " + cedula;
            stmt.execute(cmdText);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static void modificarMascota() {
        try {
            // The newInstance() call is a work around for someClass.forName("com.mysql.jdbc.Driver").newInstance();
            // broken Java implementations
            listarMascota();
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = null;
            Statement stmt = null;
            out.println("Digite el ID de la mascota que desea modificar");
            int idMascota = Integer.parseInt(in.readLine());
            out.println("Digite el nuevo nombre");
            String nombre = in.readLine();
            out.println("Digite la raza");
            String raza = in.readLine();
            int dia;
            int mes;
            int anno;
            out.println("Digite el nuevo dia ");
            dia = Integer.parseInt(in.readLine());
            out.println("digite el nuevo mes");
            mes = Integer.parseInt(in.readLine());
            out.println("Digite el nuevo anno");
            anno = Integer.parseInt(in.readLine());
            LocalDate fecNac = LocalDate.of(anno, mes, dia);

            conn = DriverManager.getConnection("jdbc:mysql://localhost/prueba?" +
                    "user=root&password=root");

            stmt = conn.createStatement();
            String cmdText = "UPDATE MASCOTA SET NOMBRE = '" +
                    nombre + "',RAZA = '" + raza + "',FEC_NACIMIENTO = '" + fecNac+
                    "' WHERE ID_MASCOTA = " + idMascota;
            stmt.execute(cmdText);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }


    public static void eliminar() {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations
            String hacer = "si";
            listar(hacer);
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = null;
            Connection conn2 = null;
            Statement stmt = null;
            Statement stmt2 = null;
            out.println("Digite lel ID de la persona que desea eliminar");
            int id = Integer.parseInt(in.readLine());
            out.println("Si elimina a esta persona, eliminara todas las mascotas asociadas, digite 1 para si o 0 para no");
            int opcion = Integer.parseInt(in.readLine());

            if (opcion == 1){
                conn = DriverManager.getConnection("jdbc:mysql://localhost/prueba?" +
                        "user=root&password=root");
                stmt = conn.createStatement();
                stmt.execute("DELETE  FROM MASCOTA  WHERE ID_DUENO = " + id);

                stmt2 = conn.createStatement();
                stmt2.execute("DELETE FROM PERSONA WHERE ID = "+id);
            }else{
                out.println("Eliminacion cancelada");
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }


    public static void eliminarMascota() {
        try {
            // The newInstance() call is a work around for some
            // broken Java implementations
            listarMascota();
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            Connection conn = null;
            Statement stmt = null;
            out.println("Digite lel ID de la mascota que desea eliminar");
            int id = Integer.parseInt(in.readLine());
            conn = DriverManager.getConnection("jdbc:mysql://localhost/prueba?" +
                    "user=root&password=root");
            stmt = conn.createStatement();
            stmt.execute("DELETE  FROM MASCOTA  WHERE ID_MASCOTA = " + id);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }




}
