import java.time.LocalDate;

public class Mascota {
    private int idMascota;
    private String nombre;
    private String raza;
    private LocalDate fecNacimiento;
    private Persona deunno;

    public Mascota() {
    }

    public Mascota(int idMascota, String nombre, String raza, LocalDate fecNacimiento, Persona deunno) {
        this.idMascota = idMascota;
        this.nombre = nombre;
        this.raza = raza;
        this.fecNacimiento = fecNacimiento;
        this.deunno = deunno;
    }

    public int getIdMascota() {
        return idMascota;
    }

    public void setIdMascota(int idMascota) {
        this.idMascota = idMascota;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getRaza() {
        return raza;
    }

    public void setRaza(String raza) {
        this.raza = raza;
    }

    public LocalDate getFecNacimiento() {
        return fecNacimiento;
    }

    public void setFecNacimiento(LocalDate fecNacimiento) {
        this.fecNacimiento = fecNacimiento;
    }

    public Persona getDeunno() {
        return deunno;
    }

    public void setDeunno(Persona deunno) {
        this.deunno = deunno;
    }

    @Override
    public String toString() {
        return "Mascota{" +
                "idMascota=" + idMascota +
                ", nombre='" + nombre + '\'' +
                ", raza='" + raza + '\'' +
                ", fecNacimiento=" + fecNacimiento +
                ", deunno=" + deunno +
                '}';
    }
}
